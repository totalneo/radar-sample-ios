//
//  AppDelegate.swift
//  Radar Sample
//
//  Created by Agustin Bialet on 1/27/17.
//  Copyright © 2017 Totalneo. All rights reserved.
//

import UIKit
import RadarMedia

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, RadarKitDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        /* We have to register to be able to send notifications or change the app's badge*/
        let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, .sound, .badge], categories: nil)
        application.registerUserNotificationSettings(settings)
        
        
        RadarMedia.instance.delegate = self
        
        /* You need to change the application identifier to the one for the app you're testing*/
        RadarMedia.instance.applicationIdentifier = "WNcTpyld83NfW1Oixg5hjFX7kF6ua9wzmXq0mVNu"
        
        RadarMedia.instance.start()
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        /* Remove all scheduled notifications and reset the badge count */
        application.cancelAllLocalNotifications()
        application.applicationIconBadgeNumber = 0
    }
    
    func application(shouldProcess message: RKAction) -> Bool {
        /*
         This method is called everytime the framework has a "relevant" message. For a message to be relevant, we have to be entering / leaving a zone (geofence or beacon), in the timeframe defined in the administrative panel, etc.
         The message can be a RKTemplate or RKMetadata.
         
         The framework makes no assupmtions about RKMetadata's content dictionary keys / values. It is a job for application developers to parse / interpret them. In this case, we only search for a 'change_background_color' key, and use the value as an hex color.
         */
        switch message.messageType {
        case .template:
            let message = message as! RKTemplate
            
            /* While not active, we cant update UI, so we just send a local notification... */
            switch UIApplication.shared.applicationState {
            case .active:
                
                /* In this case, we find if we are showing a message already, and we ignore this one if so... */
                var presentedVC = self.window?.rootViewController
                while ((presentedVC?.presentedViewController) != nil) {
                    presentedVC = presentedVC?.presentedViewController;
                }
                
                if (presentedVC is MessageViewController) {
                    // We are already showing a message... ignore...
                } else {
                    let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "messageView") as! MessageViewController
                    vc.message = message
                    self.window?.rootViewController?.present(vc, animated: true, completion: nil)
                }
                
            default:
                let localNotification = UILocalNotification()
                localNotification.fireDate = Date(timeIntervalSinceNow: 1)
                localNotification.alertBody = message.notificationText
                
                localNotification.timeZone = TimeZone.autoupdatingCurrent
                localNotification.soundName = UILocalNotificationDefaultSoundName
                if #available(iOS 8.2, *) {
                    localNotification.alertTitle = message.notificationTitle
                }
                
                //                localNotification
                localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                localNotification.category = "Message"
                UIApplication.shared.scheduleLocalNotification(localNotification)
            }
        case .metadata:
            let message = message as! RKMetadata
            if let cd = message.contentDictionary {
                print("Received metadata with content: \(cd)")
                for (key, value) in cd {
                    if (key == "change_background_color") {
                        print("Will use \(value) as background color")
                        let color = colorWith(value)
                        self.window?.rootViewController?.view.backgroundColor = color
                    }
                }
            }
        }
        
        return true
    }
    
    func onError(_ error: RKError) {
        print("Error: \(error.errorDescription!)")
    }
    
    
    // Assumes input like "00FF00" (RRGGBB).
    fileprivate func colorWith(_ hex: String) -> UIColor {
        let rgbValue = Int(hex, radix: 16)
        
        if let rgbValue = rgbValue  {
            return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgbValue & 0xFF00) >> 8)/255.0, blue: CGFloat(rgbValue & 0xFF)/255.0, alpha: 1.0)
        } else  {
            print("We cant convert \(hex) into a number, will return white color")
            return UIColor.white
        }
    }
}


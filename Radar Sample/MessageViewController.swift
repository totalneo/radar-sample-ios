//
//  RKMessageViewController.swift
//  RadarKit
//
//  Created by Agustin Bialet on 9/27/16.
//  Copyright © 2016 Totalneo SRL. All rights reserved.
//

import UIKit
import RadarMedia

open class MessageViewController: UIViewController {
    
    /// The message to be shown
    open var message: RKTemplate?
    
    @IBOutlet weak var messageTitle: UILabel!
    @IBOutlet weak var messageWebView: UIWebView!
    
    /// :nodoc:
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        switch message?.messageType {
        case .some(.template):
            messageTitle.text=message?.notificationTitle
            messageWebView?.loadHTMLString((message?.content)!, baseURL: RadarMedia.applicationDocumentsDirectory as URL)
            messageWebView?.backgroundColor = message?.contentBackgroundColor
        default:
            /* Do we go back to the previous controller? */
            log.error("Nothing to show...!")
            navigationController?.dismiss(animated: true, completion: nil)
            
        }
    }

    /// :nodoc:
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// :nodoc:
    open override var prefersStatusBarHidden : Bool {
        return true
    }

    /// :nodoc:
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
